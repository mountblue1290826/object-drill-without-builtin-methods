import { mapObject } from "../src/mapObject.js";
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//Correct Input
try {
  console.log(
    mapObject(testObject, (val, key) => {
      if (typeof val === "string") {
        return val.toUpperCase();
      }
      return val;
    })
  );
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(mapObject(undefined, "not a function"));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(mapObject(null, "not a function"));
} catch (error) {
  console.log(error.message);
}
