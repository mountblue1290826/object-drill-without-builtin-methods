import { keys } from "../src/keys.js";


const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

//Correct Input
try {
    console.log(keys(testObject))
} catch (error) {
    console.log(error.message);
}

//Incorrect Input
try {
    console.log(keys(null))
} catch (error) {
    console.log(error.message);
}