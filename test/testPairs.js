import { pairs } from "../src/pairs.js";
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//Correct Input
try {
  console.log(pairs(testObject));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(pairs(undefined));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(pairs(null));
} catch (error) {
  console.log(error.message);
}
