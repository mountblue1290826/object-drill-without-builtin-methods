import { values } from "../src/values.js";

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//Correct Input
try {
  console.log(values(testObject));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(values(undefined));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(values(null));
} catch (error) {
  console.log(error.message);
}
