import { defaults } from "../src/defaults.js";
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const addingObject = { name: "Bruce Wayne", age: 22, location: "Mumbai", isStudent: true };


//Correct Input
try {
  console.log(defaults(testObject, addingObject));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(defaults(undefined));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(defaults(null));
} catch (error) {
  console.log(error.message);
}
