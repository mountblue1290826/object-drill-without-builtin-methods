import { invert } from "../src/invert.js";
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//Correct Input
try {
  console.log(invert(testObject));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(invert(undefined));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(invert(null));
} catch (error) {
  console.log(error.message);
}
