export function values (obj){
    if(obj===undefined || obj===null){
        throw new Error("Cannot convert undefined or null to object");
    }
    let valueArray = [];
    for(let element in obj){
        valueArray.push(obj[element])
    }
    return valueArray;
}