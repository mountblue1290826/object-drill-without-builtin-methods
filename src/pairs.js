export function pairs(obj) {
  if (obj === undefined || obj === null) {
    throw new Error("Cannot convert undefined or null to object");
  }
  let resultantArray = [];
  for (let key in obj) {
    resultantArray.push([key, obj[key]]);
  }
  return resultantArray;
}
