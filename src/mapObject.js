export function mapObject(obj, cb){
    if(obj===undefined || obj===null){
        throw new Error("Cannot convert undefined or null to object");
    }
    if(typeof cb !== "function"){
        throw new Error("Second parameter must be function")
    }
    let resultObj = {};
    for(let key in obj){
        resultObj[key] = cb(obj[key], key);
    }
    return resultObj;
}