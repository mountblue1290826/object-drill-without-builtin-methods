export function keys(obj){
    if(typeof obj === undefined || obj === null){
        throw new TypeError("Cannot convert undefined or null to object")
    }
    let keyArray = [];
    for(let key in obj){
        keyArray.push(key)
    }
    return keyArray;
}