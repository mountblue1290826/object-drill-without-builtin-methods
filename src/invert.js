export function invert(obj) {
  if (obj === undefined || obj === null) {
    throw new Error("Cannot convert undefined or null to object");
  }
  let resultantObj = {};
  for (let key in obj) {
    resultantObj[obj[key]] = key;
  }
  return resultantObj;
}
