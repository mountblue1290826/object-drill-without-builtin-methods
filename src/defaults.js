export function defaults(obj, defaultProps) {
  if (
    obj === undefined ||
    obj === null ||
    defaultProps === undefined ||
    obj === null
  ) {
    throw new Error("Cannot convert undefined or null to object");
  }

  for (let key in defaultProps) {
    obj[key] = defaultProps[key];
  }
  return obj;
}
